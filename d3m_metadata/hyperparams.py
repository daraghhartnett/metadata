import abc
import typing

from pytypes import type_util

__all__ = (
    'Hyperparameter', 'Choice', 'UniformInt', 'Uniform', 'LogUniform', 'Normal', 'LogNormal',
    'Hyperparams',
)

T = typing.TypeVar('T')


class Hyperparameter(abc.ABC):
    """
    A base class for hyper-parameter descriptions.

    A base hyper-parameter does not give any information about the space of the hyper-parameter,
    besides a default value.
    """

    def __init__(self, default: typing.Any, structural_type: typing.Type, semantic_types: typing.Sequence[str] = None, description: str = None):
        if semantic_types is None:
            semantic_types = ()

        self.name = None
        self.default = default
        self.structural_type = structural_type
        self.semantic_types = semantic_types
        self.description = description

    def contribute_to_class(self, name):
        self.name = name


class Choice(Hyperparameter):
    """
    A choice hyper-parameter with a value drawn uniformly from a list of values.

    If ``None`` is a valid choice, it should be listed among ``choices``.
    """

    def __init__(self, choices: typing.Sequence[T], default: T, structural_type: typing.Type = None, semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        if structural_type is None:
            structural_type = type_util.deep_type(choices, depth=1)

        super().__init__(default, structural_type, semantic_types, description)

        self.choices = choices


class UniformInt(Hyperparameter):
    """
    An int hyper-parameter with a value drawn uniformly from ``[lower, upper)``.
    """

    def __init__(self, lower: int, upper: int, default: typing.Optional[int], semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        super().__init__(default, int, semantic_types, description)

        self.lower = lower
        self.upper = upper


class Uniform(Hyperparameter):
    """
    A float hyper-parameter with a value drawn uniformly from ``[lower, upper)``.

    If ``q`` is provided, then the value is drawn according to ``round(uniform(lower, upper) / q) * q``.
    """

    def __init__(self, lower: float, upper: float, default: typing.Optional[float], q: float = None, semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        super().__init__(default, float, semantic_types, description)

        self.lower = lower
        self.upper = upper
        self.q = q


class LogUniform(Hyperparameter):
    """
    A float hyper-parameter with a value drawn from ``[lower, upper)`` according to ``exp(uniform(lower, upper))``
    so that the logarithm of the value is uniformly distributed.

    If ``q`` is provided, then the value is drawn according to ``round(exp(uniform(lower, upper)) / q) * q``.
    """

    def __init__(self, lower: float, upper: float, default: typing.Optional[float], q: float = None, semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        super().__init__(default, float, semantic_types, description)

        self.lower = lower
        self.upper = upper
        self.q = q


class Normal(Hyperparameter):
    """
    A float hyper-parameter with a value drawn normally distributed according to ``mu`` and ``sigma``.

    If ``q`` is provided, then the value is drawn according to ``round(normal(mu, sigma) / q) * q``.
    """

    def __init__(self, mu: float, sigma: float, default: typing.Optional[float], q: float = None, semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        super().__init__(default, float, semantic_types, description)

        self.mu = mu
        self.sigma = sigma
        self.q = q


class LogNormal(Hyperparameter):
    """
    A float hyper-parameter with a value drawn according to ``exp(normal(mu, sigma))`` so that the logarithm of the value is
    normally distributed.

    If ``q`` is provided, then the value is drawn according to ``round(exp(normal(mu, sigma)) / q) * q``.
    """

    def __init__(self, mu: float, sigma: float, default: typing.Optional[float], q: float = None, semantic_types: typing.Sequence[str] = None, description: str = None) -> None:
        super().__init__(default, float, semantic_types, description)

        self.mu = mu
        self.sigma = sigma
        self.q = q


class HyperparamsMeta(abc.ABCMeta):
    """
    A metaclass which provides the hyper-parameter description its name.
    """

    def __new__(mcls, name, bases, namespace, **kwargs):
        cls = super().__new__(name, bases, namespace, **kwargs)

        for name, hyperparameter in cls.configuration:
            if not isinstance(name, str):
                raise TypeError("Hyper-parameter name is not a string: {name}".format(name=name))
            if not isinstance(hyperparameter, Hyperparameter):
                raise TypeError("Hyper-parameter description is not an instance of the Hyperparameter class: {name}".format(name=name))

            hyperparameter.contribute_to_class(name)

        return cls


class Hyperparams(metaclass=HyperparamsMeta):
    """
    A basic class to be subclassed and used as a type for ``Hyperparams``
    type argument in primitive interfaces. An instance of this subclass
    has to be passed as a ``hyperparams`` argument to primitive's constructor.

    You should subclass the class and set the ``configuration`` attribute
    to a dict, a mapping between a hyper-parameter name and its
    description as an instance of the `Hyperparameter` subclass.
    """

    configuration = {}  # type: typing.Dict[str, Hyperparameter]
