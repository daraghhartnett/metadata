import typing

from ..metadata import Metadata

__all__ = ()  # type: typing.Any

try:
    import numpy  # type: ignore

    __all__ = ('ndarray', 'matrix')

    class ndarray(numpy.ndarray):
        """
        Extended `numpy.ndarray` with the `metadata` attribute.

        `numpy.ndarray` **does not** allow directly attaching `metadata` attribute to an instance.
        """

        metadata = None  # type: Metadata

    class matrix(numpy.matrix):
        """
        Extended `numpy.matrix` with the `metadata` attribute.

        `numpy.matrix` **does** allow directly attaching `metadata` attribute to an instance.
        """

        metadata = None  # type: Metadata

    typing.Sequence.register(numpy.ndarray)  # type: ignore
    typing.Sequence.register(numpy.matrix)  # type: ignore

except ModuleNotFoundError:
    pass
