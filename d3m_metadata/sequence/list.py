import typing

from ..metadata import Metadata

__all__ = ('List',)

T = typing.TypeVar('T')


class List(typing.List[T]):
    """
    Extended Python standard `typing.List` with the `metadata` attribute.

    You have to create a subclass of this generic type with specified element type before
    you can instantiate lists.

    One can use various base and sequence types as its elements:

    * `numpy.ndarray`
    * `numpy.matrix`
    * `pandas.DataFrame`
    * `pandas.SparseDataFrame`
    * `d3m_metadata.base.Graph`
    * ``str``
    * ``bytes``
    * ``bool``
    * ``float``
    * ``int``
    * ``dict`` (consider using `typing.Dict`, `typing.NamedTuple`, or `TypedDict`)
    """

    metadata = None  # type: Metadata
