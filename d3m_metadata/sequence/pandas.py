import typing

from ..metadata import Metadata

__all__ = ()  # type: typing.Any

try:
    import pandas  # type: ignore

    __all__ = ('DataFrame', 'SparseDataFrame')

    class DataFrame(pandas.DataFrame):
        """
        Extended `pandas.DataFrame` with the `metadata` attribute.

        `pandas.DataFrame` **does** allow directly attaching `metadata` attribute to an instance.
        """

        metadata = None  # type: Metadata

    class SparseDataFrame(pandas.SparseDataFrame):
        """
        Extended `pandas.SparseDataFrame` with the `metadata` attribute.

        `pandas.SparseDataFrame` **does** allow directly attaching `metadata` attribute to an instance.
        """

        metadata = None  # type: Metadata

    typing.Sequence.register(pandas.DataFrame)  # type: ignore
    typing.Sequence.register(pandas.SparseDataFrame)  # type: ignore

except ModuleNotFoundError:
    pass
