from ..metadata import Metadata

__all__ = ('Dataset',)


class Dataset:
    """
    A class representing a dataset.
    """

    metadata = None  # type: Metadata
