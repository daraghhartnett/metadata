from typing import Any, Dict, List, Tuple, TypeVar, Union


class ALL_CHILDREN_TYPE:
    __slots__ = ()


ALL_CHILDREN = ALL_CHILDREN_TYPE()


class DIMENSION_TYPE:
    __slots__ = ()


DIMENSION = DIMENSION_TYPE()


class Metadata:
    """
    A basic class to be used as a value for `metadata` attribute
    on values passed between primitives.
    """


T = TypeVar('T', bound='Metadata')
Selector = Union[List[Union[int, str, ALL_CHILDREN, DIMENSION]], Tuple[Union[int, str, ALL_CHILDREN, DIMENSION]]]


class ValueMetadata(Metadata):
    def __init__(self, metadata: Dict[str, Any] = None) -> None:
        self._metadata = []

        if metadata:
            self._metadata.append((True, [], metadata))

    def update(self: T, is_datum: bool, selector: Selector, metadata: Dict[str, Any]) -> T:
        new_metadata = type(self)()
        new_metadata._metadata = list(self._metadata) + [(is_datum, selector, metadata)]
        return new_metadata


class PrimitiveMetadata(Metadata):
    def __init__(self, primitive: Any, metadata: Dict[str, Any]) -> None:
        self._metadata = metadata
