import abc

__all__ = ('ClusteringResult',)


class ClusteringResult(abc.ABC):
    """
    This class provides an interface of methods that all clustering result values should implement.
    """
