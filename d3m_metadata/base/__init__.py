"""
This module provides various base types one can use to represent data inside other sequences which can
then in turn be used to pass values between primitives.
"""

from .clustering import *
from .graph import *
