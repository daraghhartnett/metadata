import abc

__all__ = ('Graph',)


class Graph(abc.ABC):
    """
    This class provides an interface of methods that all graph values should implement.

    """

    def __init__(self, filename, file_type='gml', separator='\t') -> None:
        """
        Initializes the graph from a gml or a edgelist file and initializes the attributes of the class.

        Parameters
        ----------
        filename : string
            Name of the file, for example 'JohnsHopkins.edgelist' or 'JohnsHopkins.gml'.

        file_type : string
            Type of file. Currently only 'edgelist' and 'gml' are supported.
            Default = 'gml'

        separator : string
            Used if file_type = 'edgelist'.
            Default = '\t'
        """

    @abc.abstractmethod
    def get_nodes(self) -> any:
        """
        Returns the list of nodes in the graph
        """
        raise NotImplementedError("Please Implement this method")

    @abc.abstractmethod
    def get_edges(self) -> any:
        """
        Returns the list of edges in the graph
        """
        raise NotImplementedError("Please Implement this method")

    @abc.abstractmethod
    def is_directed(self) -> bool:
        """
        Returns true if the graph is directed, false otherwise
        """
        raise NotImplementedError("Please Implement this method")
