import enum
import json
import math
import os.path
from typing import Callable, Dict, Optional, TypeVar, Sequence

from sklearn import metrics, preprocessing  # type: ignore

__all__ = ('TaskType', 'TaskSubtype', 'OutputType', 'Metric', 'parse_problem_description')

CURRENT_VERSION = '2.12'


class TaskType(enum.Enum):
    """
    This is kept in sync with TA3-TA2 API TaskSubtype enum in names and values.
    """

    CLASSIFICATION = 1
    REGRESSION = 2
    SIMILARITY_MATCHING = 3
    LINK_PREDICTION = 4
    VERTEX_NOMINATION = 5
    COMMUNITY_DETECTION = 6
    GRAPH_MATCHING = 7
    TIMESERIES_FORECASTING = 8
    COLLABORATIVE_FILTERING = 9

    @classmethod
    def get_map(cls) -> dict:
        """
        Returns the map between JSON string and enum values.

        Returns
        -------
        dict
            The map.
        """

        return {
            'classification': cls.CLASSIFICATION,
            'regression': cls.REGRESSION,
            'similarityMatching': cls.SIMILARITY_MATCHING,
            'linkPrediction': cls.LINK_PREDICTION,
            'vertexNomination': cls.VERTEX_NOMINATION,
            'communityDetection': cls.COMMUNITY_DETECTION,
            'graphMatching': cls.GRAPH_MATCHING,
            'timeseriesForecasting': cls.TIMESERIES_FORECASTING,
            'collaborativeFiltering': cls.COLLABORATIVE_FILTERING,
        }

    @classmethod
    def parse(cls, name: str) -> 'TaskType':
        """
        Converts JSON string into enum value.

        Parameters
        ----------
        name : str
            JSON string.

        Returns
        -------
        TaskType
            Enum value.
        """

        return cls.get_map()[name]

    def unparse(self) -> str:
        """
        Converts enum value to JSON string.

        Returns
        -------
        str
            JSON string.
        """

        for key, value in self.get_map():
            if self == value:
                return key
        raise KeyError


class TaskSubtype(enum.Enum):
    """
    This is kept in sync with TA3-TA2 API TaskSubtype enum in names and values.
    """

    NONE = 1
    BINARY = 2
    MULTICLASS = 3
    MULTILABEL = 4
    UNIVARIATE = 5
    MULTIVARIATE = 6
    OVERLAPPING = 7
    NONOVERLAPPING = 8

    @classmethod
    def get_map(cls) -> dict:
        """
        Returns the map between JSON string and enum values.

        Returns
        -------
        dict
            The map.
        """

        return {
            None: cls.NONE,
            'binary': cls.BINARY,
            'multiClass': cls.MULTICLASS,
            'multiLabel': cls.MULTILABEL,
            'uniVariate': cls.UNIVARIATE,
            'multiVariate': cls.MULTIVARIATE,
            'overlapping': cls.OVERLAPPING,
            'nonOverlapping': cls.NONOVERLAPPING,
        }

    @classmethod
    def parse(cls, name: str) -> 'TaskSubtype':
        """
        Converts JSON string into enum value.

        Parameters
        ----------
        name : str
            JSON string.

        Returns
        -------
        TaskSubtype
            Enum value.
        """

        return cls.get_map()[name]

    def unparse(self) -> str:
        """
        Converts enum value to JSON string.

        Returns
        -------
        str
            JSON string.
        """

        for key, value in self.get_map():
            if self == value:
                return key
        raise KeyError


class OutputType(enum.Enum):
    """
    This is kept in sync with TA3-TA2 API OutputType enum in names and values.
    """

    CLASS_LABEL = 1
    PROBABILITY = 2
    REAL = 3
    NODE_ID = 4
    VECTOR_CLASS_LABEL = 5
    VECTOR_STOCHASTIC = 6
    VECTOR_REAL = 7
    FILE = 8

    @classmethod
    def get_map(cls) -> dict:
        """
        Returns the map between JSON string and enum values.

        Returns
        -------
        dict
            The map.
        """

        return {
            'classLabel': cls.CLASS_LABEL,
            'probability': cls.PROBABILITY,
            'real': cls.REAL,
            'nodeID': cls.NODE_ID,
            'vectorClassLabel': cls.VECTOR_CLASS_LABEL,
            'vectorStochastic': cls.VECTOR_STOCHASTIC,
            'vectorReal': cls.VECTOR_REAL,
            'file': cls.FILE,
        }

    @classmethod
    def parse(cls, name: str) -> 'OutputType':
        """
        Converts JSON string into enum value.

        Parameters
        ----------
        name : str
            JSON string.

        Returns
        -------
        OutputType
            Enum value.
        """

        return cls.get_map()[name]

    def unparse(self) -> str:
        """
        Converts enum value to JSON string.

        Returns
        -------
        str
            JSON string.
        """

        for key, value in self.get_map():
            if self == value:
                return key
        raise KeyError


Truth = TypeVar('Truth', bound=Sequence)
Predictions = TypeVar('Predictions', bound=Sequence)
Labels = TypeVar('Labels', bound=Sequence)


class Metric(enum.Enum):
    """
    This is kept in sync with TA3-TA2 API Metric enum in names and values.
    """

    ACCURACY = 1
    F1 = 2
    F1_MICRO = 3
    F1_MACRO = 4
    ROC_AUC = 5
    ROC_AUC_MICRO = 6
    ROC_AUC_MACRO = 7
    ROOT_MEAN_SQUARED_ERROR = 8
    ROOT_MEAN_SQUARED_ERROR_AVG = 9
    MEAN_ABSOLUTE_ERROR = 10
    R_SQUARED = 11
    NORMALIZED_MUTUAL_INFORMATION = 12
    JACCARD_SIMILARITY_SCORE = 13
    EXECUTION_TIME = 14
    MEAN_SQUARED_ERROR = 15

    @classmethod
    def get_map(cls) -> dict:
        """
        Returns the map between JSON string and enum values.

        Returns
        -------
        dict
            The map.
        """

        return {
            'accuracy': cls.ACCURACY,
            'f1': cls.F1,
            'f1Micro': cls.F1_MICRO,
            'f1Macro': cls.F1_MACRO,
            'rocAuc': cls.ROC_AUC,
            'rocAucMicro': cls.ROC_AUC_MICRO,
            'rocAucMacro': cls.ROC_AUC_MACRO,
            'meanSquaredError': cls.MEAN_SQUARED_ERROR,
            'rootMeanSquaredError': cls.ROOT_MEAN_SQUARED_ERROR,
            'rootMeanSquaredErrorAvg': cls.ROOT_MEAN_SQUARED_ERROR_AVG,
            'meanAbsoluteError': cls.MEAN_ABSOLUTE_ERROR,
            'rSquared': cls.R_SQUARED,
            'normalizedMutualInformation': cls.NORMALIZED_MUTUAL_INFORMATION,
            'jaccardSimilarityScore': cls.JACCARD_SIMILARITY_SCORE,
        }

    @classmethod
    def parse(cls, name: str) -> 'Metric':
        """
        Converts JSON string into enum value.

        Parameters
        ----------
        name : str
            JSON string.

        Returns
        -------
        Metric
            Enum value.
        """

        return cls.get_map()[name]

    def unparse(self) -> str:
        """
        Converts enum value to JSON string.

        Returns
        -------
        str
            JSON string.
        """

        for key, value in self.get_map():
            if self == value:
                return key
        raise KeyError

    def get_function(self) -> Callable[[Truth, Predictions, Optional[Labels]], float]:
        """
        Returns a function suitable for computing this metric.

        Returns
        -------
        function
            A function with (y_true, y_pred, labels=None) signature, returning float.
        """

        def binarize(fun:  Callable[[Truth, Predictions, Optional[Labels]], float]) -> Callable[[Truth, Predictions, Optional[Labels]], float]:
            def inner_f(y_true: Truth, y_score: Predictions, labels: Labels = None) -> float:
                label_binarizer = preprocessing.LabelBinarizer()

                y_true = label_binarizer.fit_transform(y_true)
                y_score = label_binarizer.transform(y_score)

                return fun(y_true, y_score, labels)

            return inner_f

        def root_mean_squared_error_avg(y_true: Truth, y_pred: Predictions, labels: Labels = None) -> float:
            error_sum = 0.0
            count = 0

            for y_t, y_p in zip(y_true, y_pred):  # type: ignore
                error_sum += math.sqrt(metrics.mean_squared_error(y_t, y_p))
                count += 1

            return error_sum / count

        functions_map = {
            self.ACCURACY: lambda y_true, y_pred, labels=None: metrics.accuracy_score(y_true, y_pred),
            # TODO: What to do if labels are not given? Passing None for pos_label is probably not good.
            self.F1: lambda y_true, y_pred, labels=None: metrics.f1_score(y_true, y_pred, pos_label=labels and labels[1]),
            self.F1_MICRO: lambda y_true, y_pred, labels=None: metrics.f1_score(y_true, y_pred, labels, average='micro'),
            self.F1_MACRO: lambda y_true, y_pred, labels=None: metrics.f1_score(y_true, y_pred, labels, average='macro'),
            self.ROC_AUC: lambda y_true, y_score, labels: metrics.roc_auc_score(y_true, y_score),
            self.ROC_AUC_MICRO: binarize(lambda y_true, y_score, labels: metrics.roc_auc_score(y_true, y_score, average='micro')),  # type: ignore
            self.ROC_AUC_MACRO: binarize(lambda y_true, y_score, labels: metrics.roc_auc_score(y_true, y_score, average='macro')),  # type: ignore
            self.MEAN_SQUARED_ERROR: lambda y_true, y_pred, labels=None: metrics.mean_squared_error(y_true, y_pred),
            self.ROOT_MEAN_SQUARED_ERROR: lambda y_true, y_pred, labels=None: math.sqrt(metrics.mean_squared_error(y_true, y_pred)),
            self.ROOT_MEAN_SQUARED_ERROR_AVG: root_mean_squared_error_avg,
            self.MEAN_ABSOLUTE_ERROR: lambda y_true, y_pred, labels=None: metrics.mean_absolute_error(y_true, y_pred),
            self.R_SQUARED: lambda y_true, y_pred, labels=None: metrics.r2_score(y_true, y_pred),
            self.NORMALIZED_MUTUAL_INFORMATION: lambda labels_true, labels_pred, labels=None: metrics.normalized_mutual_info_score(labels_true, labels_pred),
            self.JACCARD_SIMILARITY_SCORE: lambda y_true, y_pred, labels=None: metrics.jaccard_similarity_score(y_true, y_pred),
        }

        if self not in functions_map:
            raise NotImplementedError("Computing metric {metric} is not supported.".format(metric=self))

        return functions_map[self]  # type: ignore


def parse_problem_description(problem_schema_path: str) -> dict:
    """
    Parses problem description. It parses values to enums when suitable.

    Parameters
    ----------
    problem_schema_path : str
        File path to the problem description (``problemSchema.json``).

    Returns
    -------
    dict
        A parsed problem description.
    """

    with open(problem_schema_path, 'r') as problem_schema_file:
        problem_schema = json.load(problem_schema_file)

    if problem_schema.get('problemSchemaVersion', None) != CURRENT_VERSION:
        raise NotImplementedError("Only supporting problem schema whose version is {version}.".format(version=CURRENT_VERSION))

    description_filename = problem_schema.get('descriptionFile', None)
    if description_filename:
        # If description_filename is absolute, this simply returns it,
        # otherwise it makes it relative to os.path.dirname(problem_schema_path).
        description_filename = os.path.join(os.path.dirname(problem_schema_path), description_filename)

        with open(description_filename, 'r') as description_file:
            description = description_file.read()

    else:
        description = ''

    return {
        'description': description,
        'metric': Metric.parse(problem_schema['metric']),
        'output_type': OutputType.parse(problem_schema['outputType']),
        'problem_id': problem_schema['problemId'],
        'task_type': TaskType.parse(problem_schema['taskType']),
        'task_subtype': TaskSubtype.parse(problem_schema.get('taskSubType', None)),
        'target': problem_schema['target'],
    }
