{
  "id": "https://metadata.datadrivendiscovery.org/schemas/definitions.json#",
  "definitions": {
    "description": {
      "type": "string",
      "description": "A natural language description in an unspecified language."
    },
    "id": {
      "type": "string",
      "description": "A static UUID generated in any way. It should never change for a given value, even if the value itself is changing. For example, all versions of the same primitive should have the same id.",
      "pattern": "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"
    },
    "version": {
      "type": "string",
      "description": "A string representing a version. Versions are compared according to PEP 440."
    },
    "schema_version": {
      "type": "string",
      "description": "A string representing a metadata.datadrivendiscovery.org schema version to which metadata conforms."
    },
    "length": {
      "type": "integer",
      "description": "Number of elements in a given dimension (number of samples, number of columns, etc.)."
    },
    "name": {
      "type": "string",
      "description": "A human readable name in an unspecified language or format."
    },
    "installation": {
      "type": "array",
      "description": "Installation instructions for a primitive. Everything listed has to be installed for a primitive to work.",
      "items": {
        "type": "object",
        "oneOf": [
          {
            "properties": {
              "type": {
                "type": "string",
                "enum": ["pip"],
                "description": "A Python package."
              },
              "package": {
                "type": "string",
                "description": "Python package name."
              },
              "version": {
                "type": "string",
                "description": "Exact version string."
              },
              "registry": {
                "type": "string"
              }
            },
            "required": [
              "package",
              "type",
              "version"
            ]
          },
          {
            "properties": {
              "type": {
                "type": "string",
                "enum": ["docker"],
                "description": "A Docker image."
              },
              "image_name": {
                "type": "string",
                "description": "Docker image name including a label, and optionally prefixed with a registry."
              },
              "image_digest": {
                "type": "string",
                "description": "Docker image digest.",
                "pattern": "^sha256:[a-fA-F0-9]{64}$"
              }

            },
            "required": [
              "image_digest",
              "image_name",
              "type"
            ]
          },
          {
            "properties": {
              "type": {
                "type": "string",
                "enum": ["ubuntu"],
                "description": "A system package."
              },
              "package": {
                "type": "string",
                "description": "Ubuntu package name."
              },
              "version": {
                "type": "string",
                "description": "Exact version string."
              }
            },
            "required": [
              "package",
              "type",
              "version"
            ]
          }
        ],
        "additionalProperties": true
      },
      "minItems": 1
    },
    "structural_type": {
      "type": "string",
      "description": "A fully qualified Python path to type's Python class."
    },
    "mime_type": {
      "type": "string",
      "description": "MIME type of the value in its extended form defining encoding."
    },
    "stored_size": {
      "type": "string",
      "description": "Size in bytes when or if stored to disk."
    },
    "semantic_type": {
      "type": "string",
      "description": "A URI defining the semantic type."
    }
  }
}
