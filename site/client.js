import JSONSchemaView from 'json-schema-view-js';
import deref from 'deref';
import isPlainObject from 'is-plain-object';

const definitions = 'schemas/definitions.json';

const schemas = [
  'schemas/primitive.json',
  'schemas/container.json',
  'schemas/dimension.json',
  'schemas/datum.json',
];

Promise.all(schemas.map((schema) => {
  return fetch(schema, {credentials: 'same-origin'}).then((response) => {
    if (response.ok) {
      return response.json();
    }
    else {
      throw new Error("Fetch failed.");
    }
  });
})).then((fetchedSchemas) => {
  fetch(definitions, {credentials: 'same-origin'}).then((response) => {
    if (response.ok) {
      return response.json();
    }
    else {
      throw new Error("Fetch failed.");
    }
  }).then((fetchedDefinitions) => {
    renderSchemas(fetchedSchemas, fetchedDefinitions);
  });
});

function sortSchema(schema) {
  if (!isPlainObject(schema)) {
    return schema;
  }

  const sortedSchema = {};

  Object.entries(schema).sort((a, b) => {
    if (a[0] < b[0]) {
      return -1;
    }
    else if (a[0] > b[0]) {
      return 1;
    }
    else {
      return 0;
    }
  }).forEach(pair => {
    sortedSchema[pair[0]] = sortSchema(pair[1]);
  });

  return sortedSchema;
}

function renderSchemas(schemas, definitions) {
  const schemasElement = document.querySelector('.schemas');

  definitions = deref.util.normalizeSchema(definitions);

  const dereferenceSchema = deref();

  schemas = schemas.map((schema) => {
    return dereferenceSchema(schema, [definitions], true);
  });

  schemas.forEach(function (schema) {
    schema = sortSchema(schema);

    const formatter = new JSONSchemaView(schema, 7);

    const schemaWrapper = document.createElement('div');
    schemaWrapper.className = 'schema-wrapper';

    schemaWrapper.appendChild(formatter.render());
    schemasElement.appendChild(schemaWrapper);
  });
}
