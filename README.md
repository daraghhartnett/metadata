# Metadata for values and primitives

Metadata is a core component of any data-based system.
This repository is standardizing how we represent metadata in the D3M program
and focusing on three types of metadata:
* metadata associated with primitives
* metadata associated with datasets
* metadata associated with values passed inside pipelines

This repository is also standardizing types of values being passed between
primitives in pipelines.
While theoretically any value could be passed between primitives, limiting
them to a known set of values can make primitives more compatible,
efficient, and values easier to introspect by TA3 systems.

## About Data Driven Discovery Program

DARPA Data Driven Discovery (D3M) Program is researching ways to get machines to build
machine learning pipelines automatically. It is split into three layers:
TA1 (primitives), TA2 (systems which combine primitives automatically into pipelines
and executes them), and TA3 (end-users interfaces).

## Installation

This package works with Python 3.6+.

You can run
```
pip install -r requirements.txt
pip install .
```
in the root directory of this repository to install the `d3m_metadata` package.

## Changelog

See [HISTORY.md](./HISTORY.md) for summary of changes to this package.

## Sequence types

All input and output values passed between primitives should expose a `Sequence`
protocol and provide `metadata` attribute with metadata.

`d3m_metadata.sequence` module exposes such standard types:

* `ndarray` – [`numpy.ndarray`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.html) with support for `metadata` attribute
* `matrix` – [`numpy.matrix`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.matrix.html) with support for `metadata` attribute
* `DataFrame` – [`pandas.DataFrame`](https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.html) with support for `metadata` attribute
* `SparseDataFrame` – [`pandas.SparseDataFrame`](https://pandas.pydata.org/pandas-docs/stable/sparse.html#sparsedataframe) with support for `metadata` attribute
* `List[T]` – a generic [`typing.List[T]`](https://docs.python.org/3/library/typing.html#typing.List) type with support for `metadata` attribute
* `Dataset` – a class representing datasets, including D3M datasets

`List[T]` generic type can be used to create a list container which use
base types, sequence types themselves, standard primitive interface
classes, or Python builtin primitive types as its elements.

## Base types

`d3m_metadata.base` module exposes specialized base types which can be used
as individual elements in a `List[T]` container:

* `Graph` – an interface for representing graph values
* `ClusteringResult` – an interface for representing clustering results

Besides these specialized base types, and sequence types themselves and
standard primitive interface classes, also the following Python builtin
primitive types can be used:

* `str`
* `bytes`
* `bool`
* `float`
* `int`
* `dict` (consider using [`typing.Dict`](https://docs.python.org/3/library/typing.html#typing.Dict),
  [`typing.NamedTuple`](https://docs.python.org/3/library/typing.html#typing.NamedTuple), or [`TypedDict`](https://mypy.readthedocs.io/en/stable/kinds_of_types.html#typeddict))

## Metadata

`d3m_metadata.metadata` module provides a standard Python implementation for
metadata object.

Metadata can be specified for a value itself (a container value) or data
contained inside the container value by specifying a `selector` which
limits given metadata just to that part of the value. Selector is a
tuple of strings, integers, or special values. Selector corresponds
to a series of `[...]` attribute getter Python operations on the container value.

Special selector values:

* `ALL_CHILDREN` – makes metadata apply to all children in a given dimension (a wildcard)
* `DIMENSION` – makes metadata apply to a whole dimension and not to a dimension instance (can be used only
  inside dimension context, as the last element in the selector)

Metadata itself is represented as a (potentially nested) dict.
If multiple metadata dicts comes from different selectors for the
same resolved selector location, they are merged together in the order
from least specific to more specific, later overriding earlier.
`null` metadata value clears the key specified from a less specific selector.

Metadata can be specified in four different contexts:

* `primitive` – for metadata describing primitives
* `container` – for metadata describing whole container value (value passed between primitives)
* `datum` – for metadata describing datum itself (e.g., cells)
* `dimension` – for metadata describing dimension itself (e.g., rows and columns)

When specifying a selector, one has to also provide a context. Container context is mutually
exclusive with datum context: container is datum at a top-level.
Metadata for primitives do not use a selector because only metadata
for primitives themselves is possible.
As a consequence, only the following combinations are possible:

* `primitive` context
* `container` context
* `datum` context + selector
* `dimension` context + selector

### Example

To better understand how metadata is attached to various parts of the value,
we can first observe that any value can be represented as a nested data structure
using numeric and string indexes. A
[simple tabular D3M dataset](./tests/datasets/iris_dataset_1/)
could be represented as:

```yaml
{
  "0": [
    [0, 5.1, 3.5, 1.4, 0.2, "Iris-setosa"],
    [1, 4.9, 3, 1.4, 0.2, "Iris-setosa"],
    ...
  ]
}
```

It contains one resource with ID `"0"` which is the first dimension (using strings
as index),
then rows, which is the second dimension, and then columns, which is the third
dimension. The last two dimensions are numeric.
In Python, accessing third column of a second row would be
`["0"][1][2]` which would be value `3`. This is also the selector if we
would want to attach metadata to that cell (datum context). If this metadata is description
for this cell, we can thus describe this datum metadata as a pair of a selector and
a metadata dict:

* context: `datum`
* selector: `["0"][1][2]`
* metadata: `{"description": "Measured personally by Ronald Fisher."}`

Note: Metadata dicts in this example do not use [standard metadata keys](#standard-metadata-keys)
and are for demonstration purpose only.

Dataset-level metadata can be described as a container context:

* context: `container`
* metadata: `{"id": "iris_dataset_1", "name": "Iris Dataset"}`

Container context does not use a selector because it applies only to the
top-level of the value (or the value as a whole, depending how you look).

To describe first dimension itself, we can do:

* context: `dimension`
* selector: `[DIMENSION]`
* metadata: `{"length": 1, "description": "Resources in the dataset."}`

To attach metadata to the first (and only) resource, we can do:

* context: `datum`
* selector: `["0"]`
* metadata: `{"resource_path": "tables/learningDoc.csv", "resource_type": "table"}`

Rows dimension:

* context: `dimension`
* selector: `["0"][DIMENSION]`
* metadata: `{"length": 150, "description": "Rows."}`

Columns dimension:

* context: `dimension`
* selector: `["0"][ALL_CHILDREN][DIMENSION]`
* metadata: `{"length": 6, "description": "Columns."}`

Observe that there is no requirement that dimensions are aligned
from the perspective of metadata. But in this case they are, so we can
use `ALL_CHILDREN` wildcard to describe columns for all rows.

Column names belong to each particular column and not all columns,
so we can specify them like:

* context: `dimension`
* selector: `["0"][ALL_CHILDREN][2]`
* metadata: `{"name": "sepalWidth"}`

Using key `"name"` can serve to assign a string name to otherwise numeric dimension.

Observe a difference between, in dimension context:

* `["0"][ALL_CHILDREN][DIMENSION]` – metadata for the whole dimension
* `["0"][ALL_CHILDREN][ALL_CHILDREN]` – metadata for all dimension instances of a given dimension
* `["0"][ALL_CHILDREN][2]` – metadata for a particular dimension instance

Third column datum metadata:

* context: `datum`
* selector: `["0"][ALL_CHILDREN][2]`
* metadata: `{"structural_type": "real", "semantic_type": ["attribute"]}`

We attach structural and semantic types to datums themselves and not dimensions.
Because we use `ALL_CHILDREN` selector, this is internally stored efficiently.
We see traditional approach of storing this information in the header of a column
as a special case of a `ALL_CHILDREN` selector.

Note that the name of a column belongs to the dimension metadata because it is
just an alternative way to reference values in an otherwise numeric
dimension. This is different from a case where a dimension has string-based
index (a dict) where names of values are part of the data structure at that
dimension.

Fetching all metadata for `["0"][1][2]` returns:

```
{"structural_type": "real", "semantic_type": ["attribute"], "description": "Measured personally by Ronald Fisher."}
```

### Standard metadata keys

You can use custom keys for metadata, but the following keys are standardized,
so you should use those if you are trying to represent the same metadata:
[`https://metadata.datadrivendiscovery.org/schemas/definitions.json`](https://metadata.datadrivendiscovery.org/schemas/definitions.json) ([source](./d3m_metadata/schema/metadata/definitions.json))

Different keys are expected in different contexts:

* `primitive` –
  [`https://metadata.datadrivendiscovery.org/schemas/primitive.json`](https://metadata.datadrivendiscovery.org/schemas/primitive.json) ([source](./d3m_metadata/schema/metadata/primitive.json))
* `container` –
  [`https://metadata.datadrivendiscovery.org/schemas/container.json`](https://metadata.datadrivendiscovery.org/schemas/container.json) ([source](./d3m_metadata/schema/metadata/container.json))
* `dimension` –
  [`https://metadata.datadrivendiscovery.org/schemas/dimension.json`](https://metadata.datadrivendiscovery.org/schemas/dimension.json) ([source](./d3m_metadata/schema/metadata/dimension.json))
* `datum` –
  [`https://metadata.datadrivendiscovery.org/schemas/datum.json`](https://metadata.datadrivendiscovery.org/schemas/datum.json) ([source](./d3m_metadata/schema/metadata/datum.json))

A more user friendly visualizaton of schemas listed above is available at
[https://metadata.datadrivendiscovery.org/](https://metadata.datadrivendiscovery.org/).

Contribute: Standardizing metadata schemas are an ongoing process. Feel free to
contribute suggestions and merge requests with improvements.
